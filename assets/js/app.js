var $confirmationTable = $('#confirmationTable');

function createConfirmationResult() {
    $confirmationTable.children('tbody').html('');

    $('#registrationForm').find('input').each(function () {
        let displayValue, tr;

        switch (this.type) {
            case  'hidden':
                return false;
            case 'file':
                displayValue = '';
                if (this.files.length !== 0) {
                    displayValue = `<img style="max-width: 75px;" src="${URL.createObjectURL(this.files[0])}" />`;
                }
                break;
            case 'password':
                displayValue = '*****';
                break;
            default:
                displayValue = this.value;
                break;
        }
        tr = `<tr>
                    <td>${$(this).parent().children('label').text()}</td>
                    <td>${displayValue}</td>
                </tr>`;
        $confirmationTable.children('tbody').append(tr)
    });
}

$('#registrationForm').on('input', 'input, textarea', function (e) {
    createConfirmationResult();
});

createConfirmationResult();