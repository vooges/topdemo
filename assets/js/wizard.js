function activateStep($wizard, step) {
    $wizard.data('activeStep', step);

    $wizard.find('.step').removeClass('active');
    $wizard.find('.step').eq(step - 1).addClass('active');

    $wizard.find('.tabs span').removeClass('active');
    $wizard.find('.tabs span').eq(step - 1).addClass('active');
}

////////////////////////////////////////////////////////
/// Handles Wizard related tasks, like prev/next step
////////////////////////////////////////////////////////
$('.wizard').each(function () {
    $(this).data('activeStep', 1);
    $(this).data('numSteps', $(this).find('.step').length);

    $(this).on('click', '.prev', () => {
        if ($(this).data('activeStep') > 1) {
            activateStep($(this), $(this).data('activeStep') - 1);
        }
    });

    $(this).on('click', '.next', () => {
        if ($(this).data('activeStep') < $(this).data('numSteps')) {
            activateStep($(this), $(this).data('activeStep') + 1);
        }
        else if ($(this).data('activeStep') === $(this).data('numSteps')) {
            if ($(this).find('form')[0].checkValidity()) {
                $(this).find('form').submit();
            } else {
                $(this).find('form')[0].reportValidity();
                activateStep($(this), 1);
            }
        }
    })
});