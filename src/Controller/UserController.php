<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Service\FileUploader;
use App\Service\ImageService;
use App\Service\MailerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class UserController extends AbstractController
{
    protected $params;


    public function __construct(ParameterBagInterface $params)
    {
        $this->params = $params;
    }

    /**
     * @Route("/login", name="app_login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            $this->redirectToRoute('app_secret_page');
        }

        return $this->render('security/login.html.twig', [
            'last_username' => $authenticationUtils->getLastUsername(),
            'error' => $authenticationUtils->getLastAuthenticationError()
        ]);
    }

    /**
     * @Route("/logout", name="app_logout")
     * @throws \Exception
     */
    public function logout()
    {
        throw new \Exception('This method can be blank - it will be intercepted by the logout key on your firewall');
    }

    /**
     * @Route("/registreer", name="app_register")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param MailerService $mailerService
     * @param FileUploader $fileUploader
     * @param ImageService $imageService
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function register(
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder,
        MailerService $mailerService,
        FileUploader $fileUploader,
        ImageService $imageService
    ): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setEmail(strtolower($user->getEmail()));
            $user->setRegistrationIp($_SERVER['REMOTE_ADDR']);
            if ($user->getAvatar()) {
                $user = $this->processAvatar($user, $fileUploader, $imageService);
            }

            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $mailerService->sendRegistrationEmail($user);

            return $this->redirectToRoute('app_registration_complete', [
                'name' => $user->getFirstname()
            ]);
        }

        return $this->render('user/register.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param User $user
     * @param FileUploader $fileUploader
     * @param ImageService $imageService
     * @return User
     */
    private function processAvatar(User $user, FileUploader $fileUploader, ImageService $imageService)
    {
        /** @var UploadedFile $file */
        $file = $user->getAvatar();
        $fileName = $fileUploader->upload($file, $_ENV['AVATAR_UPLOAD_DIR']);

        $imgSrc = $this->params->get('kernel.project_dir') . '/public' . $_ENV['AVATAR_UPLOAD_DIR'] . $fileName;
        $imageService->correctOrientation($imgSrc);

        $user->setAvatar($fileName);

        return $user;
    }

    /**
     * @Route("/registreren-gelukt", name="app_registration_complete")
     * @param Request $request
     * @return Response
     */
    public function registrationComplete(Request $request): Response
    {
        return $this->render('user/registration-complete.html.twig', [
            'name' => $request->query->get('name')
        ]);
    }

    /**
     * @Route("/profiel", name="app_profile")
     */
    public function userProfile()
    {
        return $this->render('security/profile.html.twig', [
            'user' => $this->getUser()
        ]);
    }

}
