<?php

namespace App\Controller;

use App\Repository\UserRepository;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ResetPasswordController extends AbstractController
{
    /**
     * @Route("/wachtwoord-vergeten", name="app_forgot_password")
     * @param Request $request
     * @param UserService $userService
     * @return Response
     * @throws \Exception
     */
    public function forgotPassword(Request $request, UserService $userService): Response
    {
        if (!empty($request->request->all())) {
            $userService->handlePasswordReset($request->request->get('email'));
            $message = 'We hebben een e-mail verzonden (mits het e-mailadres bestaat)';
        }
        return $this->render('security/forgot-password.html.twig', [
            'message' => $message ?? '',
        ]);
    }

    /**
     * @Route("/wachtwoord-reset/{token}", name="app_reset_password")
     * @param string $token
     * @param UserService $userService
     * @param UserRepository $userRepo
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     * @throws \Exception
     */
    public function resetPassword(
        string $token,
        UserService $userService,
        UserRepository $userRepo,
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder
    ): Response
    {
        $user = $userRepo->findOneBy(['passwordResetToken' => $token]);

        if ($user === null) {
            $message = 'Onbekende activatie token';
        } elseif (!empty($request->request->all())) {
            $result = $userService->resetPassword($request, $user, $passwordEncoder);
            $message = $result['message'];

            if ($result['success'] === true) {
                return $this->redirectToRoute('app_profile');
            }
        }

        return $this->render('security/reset-password.html.twig', [
            'examplePassword' => UserService::EXAMPLE_PASSWORD,
            'passwordMinLength' => UserService::MIN_LENGTH_PASSWORD,
            'user' => $user,
            'message' => $message ?? '',
        ]);
    }
}