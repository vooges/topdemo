<?php

namespace App\Form;

use App\Entity\User;
use App\Service\UserService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, [
                'label' => 'Voornaam',
            ])
            ->add('lastnamePrefix', TextType::class, [
                'label' => 'Tussenvoegsel*',
                'required'=>false,
            ])
            ->add('lastname', TextType::class, [
                'label' => 'Achternaam',
            ])
            ->add('username', TextType::class, [
                'label' => 'Schermnaam',
                'constraints' => [
                    new Length([
                        'min' => 3,
                        'minMessage' => 'Je username moet minimaal {{ limit }} tekens lang zijn',
                    ]),
                ]
            ])
            ->add('phonenumber', TextType::class, [
                'label' => 'Telefoonnummer',
                'constraints' => [
                    new Length([
                        'min' => 10,
                        'minMessage' => 'Voer s.v.p. een geldig telefoonnummer in',
                        'max' => 25,
                    ]),
                ]
            ])
            ->add('email', EmailType::class, [])
            ->add('plainPassword', PasswordType::class, [
                'label' => 'Wachtwoord',
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Vul s.v.p. een wachtwoord in',
                    ]),
                    new Length([
                        'min' => UserService::MIN_LENGTH_PASSWORD,
                        'minMessage' => 'Je wachtwoord moet minimaal {{ limit }} tekens lang zijn',
                        'max' => 100,
                    ]),
                ],
            ])
            ->add('avatar', FileType::class, [
                'label' => 'Avatar (jpg)',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
