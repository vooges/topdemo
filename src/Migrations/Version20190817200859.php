<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190817200859 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_access_role DROP FOREIGN KEY FK_8AB25F91E26F8B9');
        $this->addSql('DROP TABLE access_role');
        $this->addSql('DROP TABLE user_access_role');
        $this->addSql('ALTER TABLE user ADD lastname VARCHAR(255) NOT NULL, ADD lastname_prefix VARCHAR(255) DEFAULT NULL, ADD phonenumber VARCHAR(25) NOT NULL, ADD username VARCHAR(50) NOT NULL, ADD avatar VARCHAR(255) NOT NULL, CHANGE password_reset_token password_reset_token VARCHAR(25) DEFAULT NULL, CHANGE name firstname VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE access_role (id INT AUTO_INCREMENT NOT NULL, role VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, description VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE user_access_role (user_id INT NOT NULL, access_role_id INT NOT NULL, INDEX IDX_8AB25F9A76ED395 (user_id), INDEX IDX_8AB25F91E26F8B9 (access_role_id), PRIMARY KEY(user_id, access_role_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE user_access_role ADD CONSTRAINT FK_8AB25F91E26F8B9 FOREIGN KEY (access_role_id) REFERENCES access_role (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_access_role ADD CONSTRAINT FK_8AB25F9A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user ADD name VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, DROP firstname, DROP lastname, DROP lastname_prefix, DROP phonenumber, DROP username, DROP avatar, CHANGE password_reset_token password_reset_token VARCHAR(25) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
    }
}
