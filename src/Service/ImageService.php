<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ImageService
{
    private $params;

    public function __construct(ParameterBagInterface $params)
    {
        $this->params = $params;
    }

    /**
     * @param string $fileName
     * @return void
     */
    public function correctOrientation(string $fileName): void
    {
        $command = $this->params->get('image_magick_auto_orient');
        $command = str_replace('{{FILENAME}}', $fileName, $command);
        shell_exec($command);
    }

    /**
     * @param string $fileName
     * @param string $maxSizes
     * @return void
     */
    public function limitDimensions(string $fileName, string $maxSizes): void
    {
        $command = $this->params->get('image_magick_resize');
        $command = str_replace('{{FILENAME}}', $fileName, $command);
        $command = str_replace('{{SIZE}}', $maxSizes, $command);
        shell_exec($command);
    }
}