<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

class MailerService
{
    const NO_REPLY_ADDR = 'no-reply@topdemo.nl';

    private $userRepo;
    private $templating;
    private $mailer;

    /**
     * UserService constructor.
     * @param UserRepository $userRepo
     * @param \Swift_Mailer $mailer
     * @param \Twig\Environment $templating
     */
    public function __construct(UserRepository $userRepo, \Swift_Mailer $mailer, \Twig\Environment $templating)
    {
        $this->userRepo = $userRepo;
        $this->templating = $templating;
        $this->mailer = $mailer;
    }

    /**
     * @param User $user
     * @return bool
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function sendPasswordResetEmail(User $user): bool
    {
        $emailTemplate = $this->templating->render('emails/forgot-password.html.twig', [
            'user' => $user
        ]);

        $message = (new \Swift_Message('Wachtwoord vergeten'))
            ->setFrom(self::NO_REPLY_ADDR)
            ->setTo($user->getEmail())
            ->setBody($emailTemplate, 'text/html');

        $this->mailer->send($message);

        return true;
    }

    /**
     * @param User $user
     * @return int
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function sendRegistrationEmail(User $user): int
    {
        $emailTemplate = $this->templating->render('emails/registered.html.twig', [
            'user' => $user
        ]);

        $message = (new \Swift_Message('Registreren gelukt'))
            ->setFrom(self::NO_REPLY_ADDR)
            ->setTo($user->getEmail())
            ->setBody($emailTemplate, 'text/html');

        return $this->mailer->send($message);
    }
}