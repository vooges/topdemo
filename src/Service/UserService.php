<?php

namespace App\Service;

use App\Controller\UserController;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserService
{
    private $userRepo;
    private $em;
    private $mailer;
    const MIN_LENGTH_PASSWORD = 9;
    const EXAMPLE_PASSWORD = 'Fietsbel#Vliegtuig#Muismat#Limonade';

    /**
     * UserService constructor.
     * @param UserRepository $userRepo
     * @param EntityManagerInterface $entityManager
     * @param MailerService $mailer
     */
    public function __construct(UserRepository $userRepo, EntityManagerInterface $entityManager, MailerService $mailer)
    {
        $this->userRepo = $userRepo;
        $this->em = $entityManager;
        $this->mailer = $mailer;
    }

    /**
     * @param string $email
     * @return bool
     * @throws \Exception
     */
    public function handlePasswordReset(string $email): bool
    {
        /** @var User $user */
        $user = $this->userRepo->findOneBy(['email' => $email]);
        if ($user !== null) {
            $token = bin2hex(random_bytes(12));
            $user->setPasswordResetToken($token);
            $this->em->flush();
            $this->mailer->sendPasswordResetEmail($user);
            return true;
        }
        return false;
    }

    /**
     * @param Request $request
     * @param User $user
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return array
     */
    public function resetPassword(Request $request, User $user, UserPasswordEncoderInterface $passwordEncoder): array
    {
        $password = $request->request->get('password');
        $verify = $request->request->get('password_verify');
        $success = false;

        $passwordValidityResult = $this->checkPasswordValidity($password, $verify);
        if ($passwordValidityResult !== true) {
            $msg = $passwordValidityResult;
        } else {
            $user->setPassword(
                $passwordEncoder->encodePassword($user, $password)
            );
            $this->em->flush();
            $success = true;
        }

        return [
            'success' => $success,
            'message' => $msg ?? "Wachtwoord is succesvol aangepast!"
        ];
    }

    /**
     * @param string $password
     * @param string $verify
     * @return bool|string
     */
    protected function checkPasswordValidity(string $password, string $verify)
    {
        if (strlen(trim($password)) < self::MIN_LENGTH_PASSWORD) {
            return "Nieuwe wachtwoord is te kort (min. " . self::MIN_LENGTH_PASSWORD . " tekens)";
        } elseif (trim($password) !== trim($verify)) {
            return "Wachtwoorden waren niet gelijk aan elkaar";
        } elseif (trim($password) === self::EXAMPLE_PASSWORD) {
            return "Het is niet de bedoeling dat je het voorbeeldwachtwoord gebruikt";
        }

        return true;
    }
}