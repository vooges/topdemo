<?php
namespace App\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
    private $params;

    public function __construct(ParameterBagInterface $params)
    {
        $this->params = $params;
    }

    /**
     * @param UploadedFile $file
     * @param string $targetDir
     * @return string
     */
    public function upload(UploadedFile $file, string $targetDir)
    {
        $fileName = md5(uniqid()).'.'.$file->guessExtension();
        $targetdirAbs = $this->params->get('kernel.project_dir').'/public'.$targetDir;

        try {
            $file->move(
                $targetdirAbs,
                $fileName
            );
        } catch (FileException $e) {
            // ... handle exception if something happens during file upload
        }

        return $fileName;
    }
}