##Intro
This project is set up with some of Symfony's own features, e.g.:  

- The MakerBundle (https://symfony.com/doc/current/bundles/SymfonyMakerBundle/index.html)  
- Loginform generator (https://symfony.com/doc/current/security/form_login_setup.html)  
- SwiftMailer (https://symfony.com/doc/current/email.html)  
- Webpack Encore (https://symfony.com/doc/current/frontend.html)  



##Install
- Run `composer install`
- Run `yarn build`
- Configure the proper path for `DATABASE_URL` in the `.env.local` file
- Configure the url for `MAILER_URL` in the `.env.local` file
- Run `php bin/console d:m:m` to creates tables
- It's tested with a local domain in WAMP, e.g. `www.topdemo.test`
- The image generation requires imagemagick's `convert` and `mogrify`

##Info
- Normally I would implement a user activation process, but the instruction did not require this. There is a password forget feature which has a basic example of how you could set something similiar up.
- The SwiftMailer sends its mail to a spool dir, default this is `/var/spool`. After 'sending' an e-mail, a `.message` file will be viewable there.
- The `.idea` map is included in the repository. Some people prefer this, others dont. I've just put it there so it's there, deletion is always possible.
